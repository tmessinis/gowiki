package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	prom "github.com/prometheus/client_golang/prometheus"
	promhttp "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	templates = template.Must(template.ParseFiles("index.html", "edit.html", "view.html"))
	views     = prom.NewCounterVec(
		prom.CounterOpts{
			Name: "views_total",
			Help: "Total number of views",
		},
		[]string{"title"},
	)
)

func init() {
	prom.MustRegister(views)
}

type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"

	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return &Page{
		Title: title,
		Body:  body,
	}, nil
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	p := &Page{
		Title: "Gowiki",
	}
	renderTemplate(w, "index", p)
}

func lookupHandler(w http.ResponseWriter, r *http.Request) {
	title := r.FormValue("title")
	http.Redirect(w, r, "/gowiki/view/"+title, http.StatusFound)
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/gowiki/view/"):]
	views.WithLabelValues(title).Inc()
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/gowiki/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/gowiki/edit/"):]
	p, err := loadPage(title)
	if err != nil {
		p = &Page{
			Title: title,
		}
	}

	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/gowiki/save/"):]
	body := r.FormValue("body")
	p := &Page{
		Title: title,
		Body:  []byte(body),
	}

	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/gowiki/view/"+title, http.StatusFound)
}

func metricsRootHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/gowiki/metrics/", http.StatusFound)
}

func main() {
	done := make(chan bool)

	wikiServer := http.NewServeMux()
	wikiServer.HandleFunc("/gowiki/", indexHandler)
	wikiServer.HandleFunc("/gowiki/lookup/", lookupHandler)
	wikiServer.HandleFunc("/gowiki/view/", viewHandler)
	wikiServer.HandleFunc("/gowiki/edit/", editHandler)
	wikiServer.HandleFunc("/gowiki/save/", saveHandler)

	metricsServer := http.NewServeMux()
	metricsServer.HandleFunc("/gowiki/", metricsRootHandler)
	metricsServer.Handle("/gowiki/metrics/", promhttp.Handler())

	go func() {
		log.Fatal(http.ListenAndServe(":8080", wikiServer))
	}()

	go func() {
		log.Fatal(http.ListenAndServe(":9000", metricsServer))
	}()

	<-done
}
